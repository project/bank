$content[type]  = array (
  'name' => 'Transfer',
  'type' => 'transfer',
  'description' => '',
  'title_label' => 'title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'nodefamily_max' => '0',
  'nodeprofile' => 0,
  'comment' => '0',
  'old_type' => 'transfer',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
);
$content[fields]  = array (
  0 => 
  array (
    'widget_type' => 'userreference_autocomplete',
    'label' => 'Acceptor',
    'weight' => '0',
    'reverse_link' => '0',
    'description' => '',
    'default_value_widget' => 
    array (
      'field_acceptor' => 
      array (
        0 => 
        array (
          'user_name' => '',
        ),
      ),
    ),
    'default_value_php' => '',
    'required' => '1',
    'multiple' => '0',
    'referenceable_roles' => 
    array (
      1 => 1,
      0 => 1,
      2 => false,
    ),
    'referenceable_status' => 
    array (
      1 => true,
      0 => false,
    ),
    'field_name' => 'field_acceptor',
    'field_type' => 'userreference',
    'module' => 'userreference',
    'default_value' => 
    array (
      0 => 
      array (
        'uid' => '',
        'error_field' => 'field_acceptor][0][user_name',
      ),
    ),
  ),
  1 => 
  array (
    'widget_type' => 'text',
    'label' => 'Purpose',
    'weight' => '0',
    'rows' => '1',
    'description' => '',
    'default_value_widget' => 
    array (
      'field_purpose' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
    ),
    'default_value_php' => '',
    'required' => '1',
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'field_name' => 'field_purpose',
    'field_type' => 'text',
    'module' => 'text',
  ),
  2 => 
  array (
    'widget_type' => 'number',
    'label' => 'value',
    'weight' => '0',
    'description' => '',
    'default_value_widget' => 
    array (
      'field_value' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
    ),
    'default_value_php' => '',
    'required' => '1',
    'multiple' => '0',
    'min' => '',
    'max' => '',
    'prefix' => '',
    'suffix' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'field_name' => 'field_value',
    'field_type' => 'number_decimal',
    'module' => 'number',
  ),
);
