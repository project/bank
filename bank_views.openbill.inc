<?php
$view = new stdClass();
$view->name = 'open_bill';
$view->description = '';
$view->access = array (
);
$view->view_args_php = '';
$view->page = TRUE;
$view->page_title = '';
$view->page_header = '';
$view->page_header_format = '1';
$view->page_footer = '';
$view->page_footer_format = '1';
$view->page_empty = '';
$view->page_empty_format = '1';
$view->page_type = 'node';
$view->url = 'blub';
$view->use_pager = TRUE;
$view->nodes_per_page = '10';
$view->sort = array (
);
$view->argument = array (
  array (
    'type' => 'content: field_acceptor',
    'argdefault' => '1',
    'title' => '',
    'options' => '',
    'wildcard' => '',
    'wildcard_substitution' => '',
  ),
);
$view->field = array (
);
$view->filter = array (
  array (
    'tablename' => 'node',
    'field' => 'status',
    'operator' => '=',
    'options' => '',
    'value' => '1',
  ),
  array (
    'tablename' => 'node',
    'field' => 'type',
    'operator' => 'OR',
    'options' => '',
    'value' => array (
0 => 'bill',
),
  ),
);
$view->exposed_filter = array (
);
$view->requires = array(node);
$views[$view->name] = $view;
