<?php
$view = new stdClass();
$view->name = 'konto_page_user';
$view->description = '';
$view->access = array (
);
$view->view_args_php = '';
$view->page = FALSE;
$view->page_title = '';
$view->page_header = '';
$view->page_header_format = '1';
$view->page_footer = '';
$view->page_footer_format = '1';
$view->page_empty = '';
$view->page_empty_format = '1';
$view->page_type = 'table';
$view->url = '';
$view->use_pager = TRUE;
$view->nodes_per_page = '10';
$view->sort = array (
  array (
    'tablename' => 'node',
    'field' => 'created',
    'sortorder' => 'DESC',
    'options' => 'normal',
  ),
);
$view->argument = array (
  array (
    'type' => 'uid',
    'argdefault' => '1',
    'title' => '',
    'options' => '',
    'wildcard' => '',
    'wildcard_substitution' => '',
  ),
);
$view->field = array (
  array (
    'tablename' => 'node',
    'field' => 'type',
    'label' => 'Typ',
  ),
  array (
    'tablename' => 'node_data_field_acceptor',
    'field' => 'field_acceptor_uid',
    'label' => 'von/an',
    'handler' => 'content_views_field_handler_group',
    'options' => 'default',
  ),
  array (
    'tablename' => 'node_data_field_purpose',
    'field' => 'field_purpose_value',
    'label' => 'Zweck',
    'handler' => 'content_views_field_handler_group',
    'options' => 'default',
  ),
  array (
    'tablename' => 'node_data_field_value',
    'field' => 'field_value_value',
    'label' => 'Transaktionsbetrag:',
    'handler' => 'content_views_field_handler_group',
    'options' => 'be_1',
  ),
);
$view->filter = array (
  array (
    'tablename' => 'node',
    'field' => 'type',
    'operator' => 'OR',
    'options' => '',
    'value' => array (
0 => 'bill',
1 => 'transfer',
),
  ),
);
$view->exposed_filter = array (
);
$view->requires = array(node, node_data_field_acceptor, node_data_field_purpose, node_data_field_value);
$views[$view->name] = $view;

////////////////////////////////////////////////////////

 $view = new stdClass();
  $view->name = 'konto_page_acceptor';
  $view->description = '';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->page = FALSE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = '';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'content: field_acceptor',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'label' => 'Typ',
    ),
    array (
      'tablename' => 'node_data_field_acceptor',
      'field' => 'field_acceptor_uid',
      'label' => 'von/an',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_purpose',
      'field' => 'field_purpose_value',
      'label' => 'Zweck',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_value',
      'field' => 'field_value_value',
      'label' => 'Transaktionsbetrag:',
      'handler' => 'content_views_field_handler_group',
      'options' => 'be_1',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'bill',
  1 => 'transfer',
),
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node, node_data_field_acceptor, node_data_field_purpose, node_data_field_value);
  $views[$view->name] = $view;

